
public class Cache {
	
	private static Cache instance = null;
	
	private Cache() {}

	public static Cache getInstance() {
		
		if(instance == null)
		{
			instance = new Cache();
		}	
		return instance;
	}
	
	
}
