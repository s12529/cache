import java.util.Random;


public class IntPrinter implements Runnable{
	
	private static Object token = new Object();

	@Override
	public /*synchronized*/ void run() {
		// TODO Auto-generated method stub
		
		System.out.println("Działa wątek: " + Thread.currentThread().getId());
	
		try {
		
		synchronized(token) {	
			
		for(int i = 1; i<=9; i++) {
			Random r = new Random();

				Thread.sleep(5000*(int)r.nextDouble());
				System.out.print(i);
			}
		}
		} catch (InterruptedException e) {
			e.printStackTrace();	
		}
		
	}

}
